#include <iostream>
using namespace std;
class Node
{
public:
    int data;
    Node *parent;
    Node *left;
    Node *right;
    bool color;
};
int count  = 0 ;
class RbTree
{
public:
    Node *root = NULL;
    RbTree()
    {
        this->root = NULL;
    }
    void Insert(int item)
    {
        // this is new node that we want to insert
        Node *temp = new Node();
        temp->data = item;
        temp->left = temp->right = NULL;
        temp->color = true; // the Color of new node is always red
        Node *y = NULL;
        Node *x = this->root;
        // loop for finding the null pointer where we put new node
        while (x != NULL)
        {
            y = x;
            if (temp->data < x->data)
            {
                x = x->left;
            }
            else
            {
                x = x->right;
            }
        }
        // new node parent =  to null
        temp->parent = y;
        if (y == NULL)
        {
            this->root = temp;
        }
        else if (temp->data < y->data)
        {
            y->left = temp;
        }
        else
        {
            y->right = temp;
        } 
        temp->left = NULL;
        temp->right = NULL; 
        temp->color = true; 
       RB_INSERT_FIXUP(this->root, temp);
    } 
       
    void RB_INSERT_FIXUP(Node *root, Node *z)
    {
        Node *y = NULL ;
        if (z->parent == NULL) 
        {
            z->color = false;
            return;
        }
        cout<<z->parent->color<<endl;
        while (z->parent->color == true)
        {
            // if (z->parent->color == true)    
            // {
            //     break;
            // }  
            count++;
            cout<<count<<endl;
            if (z->parent == z->parent->parent->left)
            {
                cout<<"Www"<<endl;
                y = z->parent->parent->right;
                // when the colore of new node uncle is red
                // y->color is  new node uncle
                if (y->color == true)
                {
                    cout<<"case1 " <<endl;
                    z->parent->color == false;
                    y->color = false;
                    z->parent->parent->color = true;
                    z = z->parent->parent;
                }
                // if new node is the right child of its parant node than perfom the left  rotation
                else if (z == z->parent->right)
                {
                    cout<<"Last Three Cases "<<endl;
                    z = z->parent;
                    LEFT_ROTATE(root, z);
                }
                //  change the color of parant of new node becase the color of root is black
                z->parent->color = false;
                // grandparant is red
                z->parent->parent->color = true;
                RIGHT_ROTATE(root, z->parent->parent);
            }
           
            else
            {
                if( z->parent->parent->left == NULL)
                {
                    return;
                }
                y = z->parent->parent->left;
                // when the color of new node uncle is red
                // y->color is  new node uncle
                if (y->color == true)
                {
                    // cout<<"Else case 1  " <<endl; 
                    z->parent->color == false;
                    y->color = false;
                    z->parent->parent->color = true;
                    // cout<<z->parent->parent->data<<endl;
                    z = z->parent->parent;
                    // cout<<"If case 3" <<endl;
                }
                // if new node is the right child of its parant node than perfom the left  rotation
                else if (z == z->parent->left)
                {
                    cout<<"If case   nkjjk" <<endl;
                    z = z->parent;
                    RIGHT_ROTATE(root, z);
                }
                //  change the color of parant of new node becase the color of root is black
                 cout<<"If case 3" <<endl;
                 if(z->parent == NULL) 
                 {
                    cout<<"inside the z->pret"<<endl;
                     //cout<<z->parent->data<<endl;
                 }
                   cout<<"inside the z->pret"<<endl;
                  z->parent->color = false; 
                // grandparant is red 
                z->parent->parent->color = true;
                cout<<"before rotation " <<endl;
                LEFT_ROTATE(root, z->parent->parent);
            }
        } 
        // root colore is always black
        this->root->color = false;
    }
    void LEFT_ROTATE(Node *root, Node *x)
    {
        Node *y = x->right;
        x->right = y->left;
        if (y->left != NULL)
        {
            y->left->parent = x;
        }
        y->parent = x->parent;
        if (x->parent == NULL)
        {
            root = y;
        }
        else if (x == x->parent->left)
        {
            x->parent->left = y;
        }
        else
        {
            x->parent->right = y;
        }
        y->left = x;
        x->parent = y;
    }
    void RIGHT_ROTATE(Node *root, Node *x)
    {
        Node *y = x->left;
        x->left = y->right;
        if (y->right != NULL)
        {
            y->right->parent = x;
        }
        y->parent = x->parent;
        if (x->parent == NULL)
        {
            root = y;
        }
        else if (x == x->parent->right)
        {
            x->parent->right = y;
        }
        else
        {
            x->parent->left = y;
        }
        y->right = x;
        x->parent = y;
    }
    void inOrderTraversal(Node *root)
    {
        if (root != NULL)
        {
            inOrderTraversal(root->left);
            cout << root->color<< " ";
            inOrderTraversal(root->right);
        }
    }
};
int main()
{
    RbTree *Object_One = new RbTree();
    Object_One->Insert(10);
    Object_One->Insert(5);
    Object_One->Insert(30);
    Object_One->Insert(40);
    Object_One->Insert(50);
    Object_One->Insert(60);
    Object_One->Insert(70);
    Object_One->Insert(80);
    Object_One->Insert(90);
    Object_One->inOrderTraversal(Object_One->root);
}
