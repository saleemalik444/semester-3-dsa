import matplotlib.pyplot as plt 
import pandas as pd 
df = pd.read_csv('dailySteps_merged.csv' ) 
list1 = df['ActivityDay'].values.tolist() 
list2 = df['StepTotal'].values.tolist() 
listc1 = list1
listc2 = list2 
list3 = [] 
for i in range(len(listc1)):
      sum_arry = 0
      for j in range(0,len(listc2)) :
        if listc1[i] == listc1[j] and i!=j:  
            sum_arry = sum_arry + int( listc2[j])
            listc1[j] = "0" 
            listc2[j] = "0" 
      list3.append(sum_arry) 
      
plt.title("line chart for the total number of steps on daily basis.")    
plt.xlabel("ActivityDay")
plt.ylabel("StepTotal")         
plt.plot(list1, list3) 
plt.show() 
# problem number 1 part 2
df = pd.read_csv('dailyActivity_merged.csv' ) 
list1 = df['ActivityDate'].values.tolist() 
list2 = df['TotalSteps'].values.tolist()  
listc1 = list1 
listc2 = list2 
list3 = [] 
for i in range(len(listc1)): 
      sum_arry = 0
      for j in range(0,len(listc2)) :
        if listc1[i] == listc1[j] and i!=j:  
            sum_arry = sum_arry + int( listc2[j]) 
            listc1[j] = "0"
            listc2[j] = "0" 
      list3.append(sum_arry) 
plt.title("bar chart for the daily distance covered.")    
plt.xlabel("ActivityDate")
plt.ylabel("TotalSteps")       
plt.bar(list1, list3) 
plt.show()  

#problem 1 Draw the scatter chart for the total time in the bed
df = pd.read_csv('sleepDay_merged.csv' ) 
list1 = df['SleepDay'].values.tolist() 
list2 = df['TotalTimeInBed'].values.tolist()  
listc1 = list1
listc2 = list2 
list3 = [] 
for i in range(len(listc1)):
      sum_arry = 0
      for j in range(0,len(listc2)) :
        if listc1[i] == listc1[j] and i!=j:  
            sum_arry = sum_arry + int( listc2[j])
            listc1[j] = "0"
            listc2[j] = "0" 
      list3.append(sum_arry) 
plt.title("scatter chart for the total time in the bed")   
plt.xlabel("SleepDay")
plt.ylabel("TotalTimeInBed")      
plt.scatter(list1, list3)  
plt.show() 
 



#problem 1. Draw the Pie chart for the hourly steps on the 4th December 2016. 
df = pd.read_csv('hourlySteps_merged.csv' ) 
list1 = df['ActivityHour'].values.tolist() 
list2 = df['StepTotal'].values.tolist() 
list3  = []  

for i in range(0,len(list1)):
    One_Element = list1[i].split()
    sum_arry  = 0
    if One_Element[0] == "4/12/2016":
        for j in range(0,len(list1)):
            if list1[j] == list1[i]:
                sum_arry= sum_arry + int(list2[j])   
                list2[j] = 0
        if sum_arry>0:       
          list3.append(sum_arry)
plt.title("Pie chart for the hourly steps on the 4th April 2016")        
plt.pie(list3) 
plt.show()         
                 
            
     
    
    
    
    


















