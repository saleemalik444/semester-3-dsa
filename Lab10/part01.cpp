#include<iostream> 
using namespace std;
class Node
{
        public:
    int data;
    Node *parent;
    Node *left;
    Node *right;
};
class BST
{
    public:
    BST(int arr[], int size)
    {
        for (int  i = 0; i < size; i++)
        {
            insertNode(arr[i]); 
        }      
    } 
    BST() 
    { 
         cout<<" Defalt  Constructor"<<endl;
    } 
    ~BST() 
    {
        cout<<"This Is Destructor " <<endl;
    }
    Node *insertNode(int key)  
    {
        Node *y  = NULL;
        Node *x = this->root;
        Node *z = newNode(key);
        while (x  != NULL)   
        {
            y = x;
            if (z->data<x->data) 
            {
                x = x->left;
            }
            else
            {
                x = x->right;
            }
        } 
           z->parent = y;
        if (y ==  NULL)  
        {
            root = z;
        }
        else if (z->data<y->data )
        {
            y->left = z;
        } 
        else
        {
            y->right = z;
        }
    } 
    Node *newNode(int item)  
    {
        Node *temp = new  Node();
        temp->data = item;
        temp->left = temp->right = NULL;
        return temp;
    }
    bool isEmpty()  
    {  if(root == NULL) 
       {
          return true;
       }
       else
       {
          return false;
       }

    }   
    Node* getTree() 
    {
         return this->root; 
    }
    void inOrderTraversal(Node*root)
    { 
        if (root != NULL) 
        {
            inOrderTraversal(root->left);
            cout<<root->data<<" ";
            inOrderTraversal(root->right);
        }  
    }  
    void  preOrderTraversal(Node*root)
    {
        if (root != NULL) 
        {
            cout<<root->data<<" ";
            preOrderTraversal(root->left);
            preOrderTraversal(root->right);
        }  
    }     
    void postOrderTraversal(Node*root)
   {
        if (root != NULL) 
        {
            postOrderTraversal(root->left);
            postOrderTraversal(root->right);
            cout<<root->data<<" ";
        } 
   }
    Node* findNode( int x) 
   {
        Node *z = newNode(x);
        Node *temp = this->root;
       while(temp != NULL && temp->data != x )       
       {  
               if (z->data<temp->data)  
               {
                   z = temp->left;
                       temp = temp->left;
               } 
               else 
               {
                  z = temp->right;
                     temp = temp->right;   
               }  
               if (temp == NULL )   
               {
                  cout<<"This Value is Not Found " << endl;
                  return 0;
               }  
       }  
             return z;
   } 
   TRANSPLANT ( Node*u ,Node *v)
   {
      if(u->parent == NULL)
      {
          this->root = v;
      } 
      else if (u == u->parent->left) 
      {
         u->parent->left = v;
      }
      else
      {
           u->parent->right = v;
      }
      if (v !=  NULL )  
      {
            v->parent = u->parent;
      }
   }
    Node  *TREE_MINIMUM(Node *x)
    {
        while ( x->left != NULL)  
        {
           x = x->left;
        }
        return x; 
    }  
    void deleteNode(Node*z)      
   {  
        if(z->left == NULL)  
        {
            TRANSPLANT ( z ,z->right); 
        }
        else if (z->right == NULL)  
        {
            TRANSPLANT (z,z->left) ;
        }
        else
        {
            Node *y  = TREE_MINIMUM(z->right);
            if ( y->parent != z)  
            {
                TRANSPLANT(y,y->right);
                y->right = z->right;
                y->right->parent = y;
            }
                TRANSPLANT(z,y);
                y->left  = z->left;
                y->left->parent = y; 
        }    
   }
    bool isBST(Node *root) 
    { 
        if (root == NULL)
        { 
            return true;
        } 
        if(root->left != NULL  && root->left->data>root->data)
        {
            return false;
        }                        
        if(root->right != NULL && root->right->data <root->data)
        {
            return false;
        }
        if(isBST(root->left) && isBST(root->right))
        {
            return true;
        }
        return false;
    }

   int NumberOfNodes(Node*root)
   {
    if (root == NULL)
        return 0;
        return 1 + NumberOfNodes(root->left) + NumberOfNodes(root->right);
   }
   int Height(Node *root)
   {
      if(root  == NULL)
      {
               return 0;
      }
      else
      {
            int left_height =  Height(root->left);
            int right_height = Height(root->right);
            return(max(left_height,right_height)+1);
      } 
   }
   void LeafNodes(Node* root) 
   {
         if (root == NULL)  
        {
          return ; 
        }
        if(root->left == NULL && root->right == NULL  )  
        {
              cout<<" leaf "<<root->data<<endl; 
        }
          LeafNodes(root->left);  
          LeafNodes(root->right);
   }
   void visualizeTree(Node * root) 
   {
     if(root == NULL)
     {
        return ;
     } 
        if(root->left != NULL)   
        {
              cout<<"    "<<root->data<<endl; 
              cout<<"      /"<<endl;
        }
       if(root->right != NULL )   
        {
              cout<<"     "<<root->data<<endl;
              cout<<"      \\"<<endl; 
        } 
        if(root->right != NULL)
        visualizeTree(root->right); 
        if(root->left != NULL) 
        visualizeTree(root->left); 
   } 
   private:
     Node* root = NULL; 
};

int main()
{ 
    int arry[4] = {10,20,30,40};
	BST *object_tree = new  BST(arry , 4); 
    object_tree->visualizeTree(object_tree->getTree()); 
    object_tree->LeafNodes(object_tree->getTree());   
    cout<<object_tree->Height(object_tree->getTree());
    cout<<"\n";
    object_tree->inOrderTraversal(object_tree->getTree()); 
    cout<<"\n";
    object_tree->preOrderTraversal(object_tree->getTree()); 
    cout<<"\n";
    object_tree->postOrderTraversal(object_tree->getTree()); 
    cout<<"\n";
    Node *x =  object_tree->findNode(40);    
    object_tree->deleteNode(x);  
    object_tree->inOrderTraversal(object_tree->getTree());  
    cout<<"\n";
    if(x != 0)  
    { 
        cout<<x->data; 
    }  
    cout<<"\n";
    cout<<object_tree->NumberOfNodes(object_tree->getTree());
    cout<<"\n"; 
    cout<<object_tree->isBST(object_tree->getTree())<<endl;
} 
