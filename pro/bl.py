import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd
import time 
import math 
from bs4 import BeautifulSoup
import requests
import pandas as pd 



class Welcome_Windows(QDialog):
    def __init__(self):
        super(Welcome_Windows,self).__init__()
        loadUi("Welcome.ui",self) 
        self.continueButton.clicked.connect(self.Go_Next)          
    def Go_Next(self):
        self.close()  
        self.login = Login_Page() 
        self.login.show()   
class Login_Page(QDialog):
    def __init__(self):    
        super(Login_Page,self).__init__()
        uic.loadUi("login.ui",self)    
        self.Login_butten.clicked.connect(self.Login_butten_press) 
        self.Password.setEchoMode(QtWidgets.QLineEdit.Password)   
        self.show_Password.clicked.connect(self.toggleVisibility) 
    def toggleVisibility(self):
        if self.Password.echoMode()==QLineEdit.Normal:
            self.Password.setEchoMode(QLineEdit.Password)
        else:
            self.Password.setEchoMode(QLineEdit.Normal) 
    def Login_butten_press(self): 
            if(self.Email.text()  == "saleem" and self.Password.text()  == "malik"):  
                self.close() 
                self.Profile_Form_Object  = Profile_Form("None",self.Password.text(),self.Email.text() )    
                self.Profile_Form_Object.show()  
            else:
                msg=QMessageBox()
                msg.setWindowTitle("--- Error Window---") 
                msg.setText("--------SomeThing Is Messing Please Add All Information---------") 
                msg.setIcon(QMessageBox.Information)
                msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
                font=QtGui.QFont() 
                font.setPointSize(12)
                font.setBold(True) 
                font.setWeight(75)  
                msg.setFont(font)
                msg.exec()    
class Profile_Form(QWidget):
     def __init__(self,Name,Password,Email):    
         super(Profile_Form,self).__init__() 
         uic.loadUi("Profile.ui",self)     
         self.Name_Line.setText(Name)   
         self.Password_Line.setText(Password)  
         self.Email_Line.setText(Email)    
         self.Name_Line.setEnabled(False)
         self.Password_Line.setEnabled(False)
         self.Email_Line.setEnabled(False)  
         self.Change_Info.clicked.connect(self.Change_Info_Product)   
         self.View_Product.clicked.connect(self.View_Product_ALL)   
         self.Scrap_Product.clicked.connect(self.Scrap_Product_URL)   
         self.Back.clicked.connect(self.GO_Back) 
         self.View_Profile.clicked.connect(self.view_Pro) 
     def view_Pro(self):
         msg=QMessageBox()
         msg.setWindowTitle("--- Error Window---") 
         msg.setText("Already Present")  
         msg.setIcon(QMessageBox.Information)
         msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
         font=QtGui.QFont() 
         font.setPointSize(12)
         font.setBold(True) 
         font.setWeight(75)  
         msg.setFont(font)
         msg.exec()   
     def Scrap_Product_URL(self):
         self.close() 
         self.Scrap_Prouct_Object =   Scrap_Product_All()
         self.Scrap_Prouct_Object.show()   
     def Change_Info_Product(self):
         self.close() 
         self.change_Product_Informaion_Object =  Change_Information() 
         self.change_Product_Informaion_Object.show()   
     def View_Product_ALL(self):
         self.close()
         self.View_Product_Object = View_Product() 
         self.View_Product_Object.show()   
     def GO_Back(self):
             self.close()  
             self.login = Login_Page() 
             self.login.show()        
             
class Edit_Window(QWidget):
    def __init__(self, Name,Price,Brand , Shiping_Charges,Type):  
        super(Edit_Window,self).__init__()   
        uic.loadUi("edit_UI.ui",self)     
        self.name = Name
        self.price = Price
        self.brand = Brand
        self.Shiping_Charges = Shiping_Charges
        self.type = Type 
        self.Name_Line.setText(Name)   
        self.price_Line.setText(Price)  
        self.Brand_Line.setText(Brand) 
        self.Type_Line.setText(Type)  
        self.Shipping_Charges_Line.setText(Shiping_Charges)   
        self.Edit_Butten.clicked.connect(self.Edit_Product)    
        self.Arry = [ ]
    def Edit_Product(self):
        if (self.Name_Line.text()  != '' and self.price_Line.text()  !=  ''  and    self.Brand_Line.text()  !=  '' and   self.Type_Line.text()  !=  '' and self.Shipping_Charges_Line.text()  !=   ''):
            
             if (self.Name_Line.text()   == self.name and self.price_Line.text()  ==  self.price and    self.Brand_Line.text()  ==  self.brand and   self.Type_Line.text()  ==  self.type and   self.Shipping_Charges_Line.text()  == self.Shiping_Charges):
                
                     QMessageBox.about(self, "Infarmation", "This Product Is Already Present")  
                
             else: 
                 QMessageBox.about(self, "Infarmation", "This Product Is Updated ")  
                 self.close()    
                 # Arry.append( self.Name_Line.Text())
                 # Arry.append(self.price_Line.Text()    )
                 # Arry.append( self.Brand_Line.Text()  )
                 # Arry.append( self.Type_Line.Text()  )
                 # Arry.append( self.Shipping_Charges_Line.Text()) 
        else:
            QMessageBox.about(self, "Infarmation", "Something Is Messing ") 
           
class Change_Information(QWidget):         
     def __init__(self):      
         super(Change_Information,self).__init__() 
         uic.loadUi("Change Product Information.ui",self) 
         Data = load_table_As_Arry()            
         self.Set_Value_in_Table_With_Edit_Delete_Butten(Data,self.table_Show_Product)   
         self.change_Info.clicked.connect(self.Change_Info_Product)     
         self.view_product.clicked.connect(self.View_Product_ALL)   
         self.Scrap_Product.clicked.connect(self.Scrap_Product_URL)    
         self.Back.clicked.connect(self.GO_Back)   
         self.View_Profile.clicked.connect(self.view_Pro)  
         self.table_Show_Product.clicked.connect(self.handleButtonClicked) 
     def  handleButtonClicked(self):  
         current_row = self.table_Show_Product.currentRow()  
         current_column = self.table_Show_Product.currentColumn() 
         if  current_column == 0:
             self.table_Show_Product.removeRow(current_row) 
             QMessageBox.about(self, "Infarmation", "Delete Colume " )      
         elif   current_column == 1:
                Name = self.table_Show_Product.item(current_row, 2).text() 
                Brand = self.table_Show_Product.item(current_row, 3).text()  
                Price = self.table_Show_Product.item(current_row, 4).text()  
                Shiping_Charges = self.table_Show_Product.item(current_row, 5).text()  
                typ =  self.table_Show_Product.item(current_row, 8).text()   
                self.Edit_Window_Object = Edit_Window(Name ,  Price,Brand , Shiping_Charges ,typ)  
                self.Edit_Window_Object.show()                     
     def Set_Value_in_Table_With_Edit_Delete_Butten(self , Data ,Table_Name):  
          self.table_Show_Product.setRowCount(len(Data))    
          row_Index = 1 
          for value in range(1,len(Data)):   
                   self.btn_Edit = ('Edit')  
                   self.btn_Delete = ('Delete') 
                   self.table_Show_Product.setItem(row_Index ,0, QTableWidgetItem(self.btn_Delete))    
                   self.table_Show_Product.setItem(row_Index ,1, QTableWidgetItem(self.btn_Edit) )  
                   col_index  = 2 
                   arry =  Data[value]    
                   for i in range(1,len(arry)):    
                       self.table_Show_Product.setItem(row_Index,col_index,QTableWidgetItem(arry[i]))   
                       col_index +=1
                   row_Index+=1 
     def view_Pro(self):
             msg=QMessageBox()
             msg.setWindowTitle("--- Error Window---") 
             msg.setText("Already Present")  
             msg.setIcon(QMessageBox.Information)
             msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
             font=QtGui.QFont() 
             font.setPointSize(12)
             font.setBold(True) 
             font.setWeight(75)  
             msg.setFont(font)
             msg.exec()   
     def Scrap_Product_URL(self):
             self.close() 
             self.Scrap_Prouct_Object =   Scrap_Product_All()
             self.Scrap_Prouct_Object.show()   
     def Change_Info_Product(self):
             self.close() 
             self.change_Product_Informaion_Object =  Change_Information() 
             self.change_Product_Informaion_Object.show()  
     def View_Product_ALL(self):
             self.close()
             self.View_Product_Object = View_Product() 
             self.View_Product_Object.show()  
     def GO_Back(self):
                 self.close()  
                 self.login = Login_Page() 
                 self.login.show()     
class View_Product(QWidget): 
     def __init__(self):     
         super(View_Product,self).__init__() 
         uic.loadUi("all Products.ui",self)   
         self.arry  = load_table_As_Arry()  
         Set_Value_in_Table(self.arry , self.table_Show_Product) 
         self.change_Info.clicked.connect(self.Change_Info_Product)     
         self.view_product.clicked.connect(self.View_Product_ALL)   
         self.Scrap_Product.clicked.connect(self.Scrap_Product_URL)   
         self.Back.clicked.connect(self.GO_Back) 
         self.View_Profile.clicked.connect(self.view_Pro)  
         self.cobox_sort_With.activated.connect(self.get_text)     
         self.Sort_By_Combo_Box.activated.connect(self.get_text)    
     def get_text(self): 
          # ctext = self.cobox_sort_With.currentText() #how to get the text form combo box 
          self.col_index   = self.cobox_sort_With.currentIndex()    
          sortby = self.Sort_By_Combo_Box.currentText() 
          self.start_Time = time.time()   
          #Sort Algoritham
          if  self.col_index  !=0 and sortby != "Sort By" :           
              if  sortby ==  "Insertion Sort": 
                  self.Insertion_Sort(self.col_index)   
              elif sortby ==  "heap Sort":
                  self.Heapsort(self.arry,self.col_index )   
              elif sortby ==  "Selection sort":
                  self.selection(self.arry,self.col_index)   
              elif  sortby ==  "Quick Sort":
                  self.Quick_Sort(self.arry,1,len(self.arry)-1,self.col_index)  
              elif sortby ==  "bubble Sort":
                  self.bubble(self.col_index)    
              elif  sortby ==  "shell sort":    
                  self.shell_sort(self.arry ,len(self.arry) ,self.col_index)   
              elif  sortby ==  "Merge Sort":    
                      self.MergeSort(self.arry,0,len((self.arry)), self.col_index)
              elif  sortby ==  "Counting Sort": 
                  if self.col_index == 3:
                      self.arry = self.Counting_New(self.arry, self.col_index)      
                  else:
                      QMessageBox.about(self, "Infarmation", "Counting Sort Apply On integer") 
              elif  sortby ==  "Pigeonhole sorting": 
                 if self.col_index == 3:
                     self.arry = self.Pigeonhole_sorting(self.arry, self.col_index)      
                 else:
                     QMessageBox.about(self, "Infarmation", "Pigeonhole sorting Apply On integer")
              elif  sortby ==  "Radix Sort":  
                  if self.col_index == 3:
                      self.arry = self.Radix_Sort(self.arry, self.col_index)      
                  else:
                      QMessageBox.about(self, "Infarmation", "Radix Sort Apply On integer")        
              self.End_Time  = time.time()    
              self.Total_time = self.End_Time - self.start_Time  
              self.Time_Line_Edit.setText(str(self.Total_time))  
              Set_Value_in_Table(self.arry ,self.table_Show_Product)      
          else:
              if self.col_index == 0:
                  QMessageBox.about(self, "Infarmation", "Please Select One Item (Price ,Name,Brand and Type) To Sort Data ") 
              elif sortby == "Sort By":    
                  QMessageBox.about(self, "Infarmation", "Please Select One Sorting Algoritham" ) 
     def Radix_Sort( self,Arry,col_index): 
         col_Price = [val[col_index] for val in Arry]   
         max_Element = max(col_Price) 
         div = 1 
         while max_Element//div>1:
              self.Counting_Sort_Radix(Arry,div,col_index)       
              div*=10
         return Arry     
     def Counting_Sort_Radix(self,Arry,div,col_index):
          count  = [0]*10
          Output_Arry = [0]*len(Arry)    
          for i in range(0,len(Arry)):
              count[(Arry[i][col_index]//div)%10]+=1   
          for i in range(1,10): 
              count[i] += count[i-1]  
          for i in range(len(Arry)-1,-1,-1): 
              Output_Arry[count[(Arry[i][col_index]//div)%10]-1] = Arry[i] 
              count[(Arry[i][col_index]//div)%10] -=1  
          for i in range(len(Arry)): 
              Arry[i] =Output_Arry[i] 
          return Arry    
     def Pigeonhole_sorting(self,Arry, col_index) : 
          col_Price = [val[self.col_index] for val in Arry]   
          Max_Element = max(col_Price) 
          Min_Elemenet = min(col_Price)  
          rang = Max_Element - Min_Elemenet + 1  
          Count = [0]*(rang)   
          Output = [0]*len(Arry) 
          for i in range(0,len(Arry)):   
              Count[Arry[i][col_index]-Min_Elemenet] +=1   
          for i in range(1,len(Count)):  
              Count[i] += Count[i-1] 
          for i in range(len(Arry)-1,-1,-1):
              j = Count[Arry[i][col_index]-Min_Elemenet] - 1  
              Output[j] = Arry[i]  
              Count[Arry[i][col_index]-Min_Elemenet] -=1  
          return (Output)  
     def Counting_New(self,A , Col_index):  
          col_Price = [val[self.col_index] for val in A]            
          Max_Element = max(col_Price)  
          rang = Max_Element + 1 
          C=[0]*rang 
          B=[0]*len(A) 
          for i in range(len(A)):
             C[A[i][Col_index]]+=1  
          for i in range(1,len(C)):
             C[i]=C[i]+C[i-1]
          for i in range(len(A)-1,-1,-1):
             j = C[A[i][Col_index]]-1 
             B[j]=A[i]
             C[A[i][Col_index]]-=1 
          return (B) 
     def MergeSort(self,arry,p ,r, col_index): 
        if p  !=  r : 
            q = math.floor((p+r)/2)
            self.MergeSort(arry,p,q,col_index)  
            self.MergeSort(arry, q+1, r,col_index)   
            self.Merge(arry,p,q,r,col_index)    
        return arry  
     def Merge(self,arry,p,q,r ,col_index ):  
        n1  = int(q-p) 
        n2  = int(r-q) 
        left_arry  = []  
        right_arry = [] 
        for i in range(0,n1):
            left_arry.append(arry[i+p])   
        for i in range(0,n2):
            right_arry.append(arry[i+q])    
        i = 0  
        j = 0 
        k = p  
       # self.selection(left_arry,col_index)  
       # self.selection(right_arry,col_index)    
        while i<len(left_arry) and j<len(right_arry) : 
            if left_arry[i][col_index]<right_arry[j][col_index]:  
                arry[k] = left_arry[i] 
                i +=1
            else:
                arry[k] = right_arry[j]
                j +=1 
            k +=1               
        while i < len(left_arry):
                arry[k] = left_arry[i]
                i += 1
                k += 1
        while j < len(right_arry): 
            arry[k] = right_arry[j]
            j += 1
            k += 1
        #shell sorting  
     def shell_sort(self,inp, n,col_index): 
        h = n // 2 
        while h > 0:
            for i in range(h, n):
                t = inp[i] 
                j = i
                while j >= h and (inp[j - h][col_index]) > (inp[i][col_index]): 
                        inp[j] = inp[j - h]  
                        j -= h  
                inp[j] = t
            h = h // 2
      #heap sort is       
     def Heapify(self,array, n, i ,col_index ):  
         maximum = i
         left_Child = 2*i +1 
         Right_Child = 2*i + 2
         if left_Child <n  and (array[i][col_index]) <(array[left_Child][col_index]): 
                     maximum = left_Child  
         if Right_Child <n and (array[maximum][col_index])<(array[Right_Child][col_index]):    
                     maximum = Right_Child     
         if maximum != i : 
             temp = array[i]  
             array[i] = array[maximum]
             array[maximum] = temp  
             self.Heapify(array, n, maximum,col_index)          
     def Heapsort(self,arry,col_index) :
         n  = len(arry)  
         for i in range(int(n//2),-1,-1):    
             self.Heapify(arry, n ,i,col_index)     
         for i in range(n-1,0,-1):
             temp =  arry[0]  
             arry[0] = arry[i] 
             arry[i] = temp    
             self.Heapify(arry, i, 0,col_index)        
     def Insertion_Sort(self,col_index) :  
         #Complete Insertion Sort 
              for i in range(1,len(self.arry)):
                  Arry_Inside_As_key = self.arry[i] 
                  j = i -1 
                  for j in range(len(self.arry)): 
                      Arry_Inside  =  self.arry[j]  
                      if (Arry_Inside_As_key[col_index])<  (self.arry[j][col_index]):    
                              temp = self.arry[j] 
                              self.arry[j] = self.arry[i] 
                              self.arry[i] = temp      
     def selection(self,arry,col_index):  
      #complete selection sort 
       for i in range(1,len(arry)):
               min_index = i    
               for j in range(i+1,len(arry)):  
                   Arry_Inside = arry[j] 
                   Arry_Inside_At_Min_index = arry[min_index] 
                   if col_index != 0: 
                       if (Arry_Inside[col_index])<  (Arry_Inside_At_Min_index[col_index]):     # sorting by price 
                               min_index = j 
                               Arry_Inside_At_Min_index = arry[j]       
               temp = arry[min_index] 
               arry[min_index] = arry[i] 
               arry[i] = temp      
     def partision(self,Arry,Start,End,col_index):
         #partision function in Arry 
              Pivoit = Arry[End][col_index]   
              p = Start - 1  
              for i in range(Start,End):
                  if col_index != 0:  
                      if (Pivoit)>= (Arry[i][col_index]): 
                          p+=1  
                          temp = Arry[i]
                          Arry[i] = Arry[p]
                          Arry[p] = temp 
              temp = Arry[End]  
              Arry[End] = Arry[p+1]  
              Arry[p+1] = temp 
              return(p+1)  
     def Quick_Sort(self,Arry,Start,End , col_index):  
              if Start<End:
                 location_Pivoit =  self.partision(Arry,Start,End,col_index) 
                 self.Quick_Sort(Arry,Start,location_Pivoit-1,col_index)  
                 self.Quick_Sort(Arry,location_Pivoit+1 ,End,col_index) 
     def bubble(self,col_index) :
      #complete bubble  Sort 
       for  i  in range(len(self.arry )): 
              for j in range(1,len(self.arry )-1):  
                  Arry_Inside = self.arry[j]  
                  Arry_Inside_Next = self.arry[j+1]  
                  if col_index != 0 :
                        if (Arry_Inside[col_index])>(Arry_Inside_Next[col_index]):  
                           temp  = self.arry[j] 
                           self.arry[j] = self.arry[j+1]  
                           self.arry[j+1] = temp 
     def view_Pro(self): 
             msg=QMessageBox() 
             msg.setWindowTitle("--- Error Window---") 
             msg.setText("Already Present")  
             msg.setIcon(QMessageBox.Information) 
             msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
             font=QtGui.QFont() 
             font.setPointSize(12)
             font.setBold(True) 
             font.setWeight(75)  
             msg.setFont(font)
             msg.exec()   
     def Scrap_Product_URL(self):
             self.close() 
             self.Scrap_Prouct_Object =   Scrap_Product_All()
             self.Scrap_Prouct_Object.show()    
     def Change_Info_Product(self):
             self.close() 
             self.change_Product_Informaion_Object =  Change_Information() 
             self.change_Product_Informaion_Object.show()  
     def View_Product_ALL(self):
             self.close() 
             self.View_Product_Object = View_Product() 
             self.View_Product_Object.show()  
     def GO_Back(self):
                 self.close()  
                 self.login = Login_Page() 
                 self.login.show()     
     # def load_table(self,Table_Name):
     #       with open('sample_uniqe.csv', "r",encoding="utf-8") as fileInput:
     #           roww = 0
     #           data = list(csv.reader(fileInput)) 
     #           data.remove(data[0])   
     #           Table_Name.setRowCount(len(data)) 
     #           row_Index = 1 
     #           for value in range(0,len(data)):  
     #               col_index  = 0
     #               arry =  data[value]  
     #               for i in range(1,len(data[value])): 
     #                   if i == 3:
     #                       price  = (arry[3].split('.'))[0]     
     #                       price = price.replace(',','')      
     #                       price = int(int(price)*221) 
     #                       data[value][3]    = price 
     #                       print(data[value][3]) 
     #                   Table_Name.setItem(row_Index,col_index,QTableWidgetItem(str((arry[i])))) 
     #                   col_index +=1
     #               row_Index+=1    
     #       return data         
class Scrap_Product_All(QWidget): 
    
    def __init__(self):     
        super(Scrap_Product_All,self).__init__() 
        uic.loadUi("Scrap Products.ui",self)   
        self.sortby = self.Search_Site.currentText() 
        print(self.sortby)  
        self.row_Index = 0 
        self.Search_URL.clicked.connect(self.Search_URL_Clik)  
        self.change_Info.clicked.connect(self.Change_Info_Product)     
        self.view_product.clicked.connect(self.View_Product_ALL)   
        self.Scrap_Product.clicked.connect(self.Scrap_Product_URL)    
        self.Back.clicked.connect(self.GO_Back)  
        self.View_Profile.clicked.connect(self.view_Pro) 
        # self.table_Show_Product.setRowCount(self.row_Index) 
    def Search_URL_Clik(self):
          site =  self.URL_line.text() 
          if site == 'Search URL' and    self.Search_Site.currentText()  == 'Select Site': 
                   QMessageBox.about(self, "Infarmation", "Please Enter URL Of Web" ) 
          else:
              if   self.Search_Site.currentText() != 'Select Site':  
                  print(self.Search_Site.currentText())
                  Search  =  self.Search_Site.currentText()
                  url  = 'https://www.flipkart.com/search?q='+Search+'&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&' 
                  self.URL_line.setText(url) 
                  print(self.URL_line.setText(url) ) 
                  self.Search_Function(url) 
              elif  site != 'Search URL':    
                  self.Search_Function(site)     
    def Search_Function(self,site_URL): 
# url  = 'https://www.flipkart.com/search?q=laptop&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&' 
        
        url = site_URL 
        index = 0 
        product = [] 
        price =  [] 
        rating  =  [] 
        brand =  [] 
        tpye_product =  []  
        sold = [] 
        topRatedSeller  = []
        diliver_charge =  []  
        Shiping_Charges = []    
        for i in range(5) : 
          page=requests.get(url)  
          soup =  BeautifulSoup(page.content , "html.parser") 
          tpye =  soup.find('a',attrs = {'class':'_1jJQdf _2Mji8F'}) 
          for a in soup.findAll('a',href = True,attrs = {'class' : '_1fQZEK'}):
              name = a.find('div',attrs = {'class':'_4rR01T'}) 
              p = a.find('div',attrs = {'class':'_30jeq3 _1_WHN1'})
              d = a.find('div',attrs = {'class':'_3Ay6Sb'})  
              d_charge = a.find('div',attrs = {'class':'_2Tpdn3'})
              if d != None:
                    d = d.text 
              else:
                    d = 0    
              if d_charge != None:
                  diliver_charge.append(d_charge.text)       
              else:
                  diliver_charge.append('NA')
              if p != None : 
                  p = str(p.text)    
                  p = 'RS ' + p[1:] 
              r = a.find('div',attrs = {'class':'_3LWZlK'})       
              product.append(name.text)   
              price.append(p)    
              if r != None :  
                  rating.append(r.text) 
              else :
                  rating.append("0")       
              tpye_product.append(tpye.text)         
              n  = str(name.text).split(" ") 
              brand.append(n[0])     
              sold.append('NA')    
              topRatedSeller.append(" Not Top Rated Seller")  
              Shiping_Charges.append('NA') 
              if self.row_Index > self.table_Show_Product.rowCount()-1: 
                  self.table_Show_Product.insertRow(self.table_Show_Product.rowCount())    
              self.table_Show_Product.setItem(self.row_Index,0, QTableWidgetItem(((product[index]))))          
              self.table_Show_Product.setItem(self.row_Index,1,QTableWidgetItem(((brand[index])))) 
              self.table_Show_Product.setItem(self.row_Index,2,QTableWidgetItem(((price[index]))))  
              self.table_Show_Product.setItem(self.row_Index,3,QTableWidgetItem(((Shiping_Charges[index]))))   
              self.table_Show_Product.setItem(self.row_Index,4,QTableWidgetItem(((sold[index])))) 
              self.table_Show_Product.setItem(self.row_Index,5,QTableWidgetItem(((diliver_charge[index] ))))  
              self.table_Show_Product.setItem(self.row_Index,6,QTableWidgetItem(((tpye_product[index]))))  
              self.table_Show_Product.setItem(self.row_Index,7,QTableWidgetItem(((rating[index])))) 
              self.table_Show_Product.setItem(self.row_Index,8,QTableWidgetItem(((topRatedSeller[index]))))  
              self.table_Show_Product.setItem(self.row_Index,9,QTableWidgetItem((("Yes"))))  
              index +=1 
              time.sleep(1) 
              QApplication.processEvents() 
              self.row_Index+=1   
          print("loop ",i) 
          soup = soup.find('nav',attrs = {'class':'yFHi8N'})  
          url_all = [] 
          r = soup.find('a',attrs = {'class':'_1LKTO3'})  
          url  = r.get('href') 
          url =  'https://www.flipkart.com' + url    
    def view_Pro(self):
             msg=QMessageBox()
             msg.setWindowTitle("--- Error Window---") 
             msg.setText("Already Present")  
             msg.setIcon(QMessageBox.Information)
             msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
             font=QtGui.QFont() 
             font.setPointSize(12)
             font.setBold(True) 
             font.setWeight(75)  
             msg.setFont(font)
             msg.exec()   
    def Scrap_Product_URL(self):
             self.close() 
             self.Scrap_Prouct_Object =   Scrap_Product_All()
             self.Scrap_Prouct_Object.show()  
    def Change_Info_Product(self):
             self.close() 
             self.change_Product_Informaion_Object =  Change_Information() 
             self.change_Product_Informaion_Object.show()  
    def View_Product_ALL(self):
             self.close()
             self.View_Product_Object = View_Product() 
             self.View_Product_Object.show()  
    def GO_Back(self):
                 self.close()  
                 self.login = Login_Page() 
                 self.login.show()     
def Set_Value_in_Table(sorted_Arry , Table_Name):
     Table_Name.setRowCount(len(sorted_Arry)) 
     row_Index = 1 
     for value in range(1,len(sorted_Arry)):  
              col_index  = 0
              arry =  sorted_Arry[value]  
              for i in range(1,len(sorted_Arry[value])): 
                       Table_Name.setItem(row_Index,col_index,QTableWidgetItem(str((arry[i]))))   
                       col_index +=1
              row_Index+=1
def load_table_As_Arry(): 
           with open('sample_uniqe.csv', "r",encoding="utf-8") as fileInput:
               roww = 0
               data = list(csv.reader(fileInput)) 
               data.remove(data[0])   
               row_Index = 1  
               # for value in range(0,len(data)):  
               #      col_index  = 0
               #      arry =  data[value]   
               #      for i in range(1,len(data[value])): 
               #          if i == 3:
               #              price  = (arry[3].split('.'))[0]     
               #              price = price.replace(',','')      
               #              price = int(int(price)*221) 
               #              data[value][3]    = price 
               #              print(data[value][3]) 
               #          col_index +=1
               #      row_Index+=1    
           return data    
         
            
 

 






         
     
    
    
    
    
    
    
        
     