def factorial(n):
     if (n == 0):
         return 1
     else:
         return n*factorial(n-1)
     
import time
start_time = time.time() 
n = 500
ans  = factorial(n)
end_time = time.time()
run_time = end_time  - start_time
print("runing time of this function at" , n,"is" ,run_time, "seconds" )


 
     