#include <iostream>
using namespace std;
//Node Class:
class Node
{  
    public: 
    int data;
    Node *Next; 
};
Class linked list 
class link_list
{    public:
    Node *first_Element = NULL;
//Insert at Head of linked list function 
void input_At_first_index(int key) 
{
    Node  *P = new  Node;  
    P->data = key;
    P->Next = NULL;  
    if(first_Element == NULL)
    {
        first_Element = P; 
        cout<<"Element Is Added: "<<endl;
    } 
    else 
    {  
       P->Next = first_Element;
       first_Element = P;  
       cout<<"First index " <<endl; 
    }
}  
//Insert at Tail  of linked list function 
void   input_At_Tail(int key)
{   
         Node *temp = first_Element;
            Node  *record = new  Node;  
		    record->data = key; 
		    record->Next = NULL; 
       while(temp->Next !=NULL)  
       {
           temp =  temp->Next; 
       } 
       temp->Next = record;
       temp  = record;
    cout<<"Element is added at last index" <<endl;
}
//Search key in   linked list function 
 Node *Search_Element(int key) 
 {
     Node *temp = first_Element;
     while(temp != NULL)
     { 
         if (temp->data == key)
         {
             return temp;
         }
         temp = temp->Next;
     } 
 } 
// Insert at Special Location  of linked list function 
void Insert_At_Special_location(int key , int location) 
{
            Node  *record = new  Node;  
		    record->data = key; 
		    record->Next = NULL; 
		     Node *temp = first_Element;
		     int count  = 0 ;
     while(temp != NULL)
     { 
         count++;
         if (count == location)   
         {
            Node  *privious = temp;
            Node  *Next =  temp->Next;
            privious->Next  = record;
            record->Next = Next;
            break;
            
         } 
         temp = temp->Next;
     }
          if (count != location )
         {
         	cout<<"This Location Is Not Found " <<endl;
 		 } 
}          
// Delete Key In linked list function  
bool Delete_key(int key)
{ 
	   Node *temp = first_Element;
	   Node *prev = first_Element;
        while (temp != NULL) 
        {
            if (temp->data == key && temp == prev)
            { 
                first_Element = first_Element->Next;
                delete temp;
                break;
            } 
            else if (temp->data == key ) 
            {
                prev->Next = temp->Next;
                delete temp;
                break;
            } 
            prev = temp;
            temp = temp->Next;  
        }
       return false;
} 

//Update Key In linked list function  
void Update(int key , int location) 
{ 
		Node *temp = first_Element;
		int count  = 0 ;
     while(temp != NULL) 
     { 
	         if (count == location)   
	         { 
	            temp->data  = key; 
	            cout<<"Value Is Updated " << endl; 
	            break;
	         } 
	         count++;
        	 temp = temp->Next;
        }  
          if (count != location )
         {
         	cout<<"This Location Is Not Found " <<endl;
 		 } 
} 
//Print  linked list function  
void print()
{
	    Node *temp_n = first_Element; 
       while(temp_n !=NULL)  
       { 
       	    cout<<"Value  = "<<temp_n->data<<endl;
           temp_n =  temp_n->Next;  
       } 
 } 
 

};    

//  Main function in Program:
int main() 
{
	link_list *object = new link_list;
    object->input_At_first_index(1);
    object->input_At_first_index(2);
    object->input_At_first_index(3);
    object->input_At_first_index(4);
	object->Insert_At_Special_location(44,1);     
	object->input_At_Tail(5);
	object->Delete_key(2); 
    object->Update(45,1); 
    object->print();
    Node *n   = object->Search_Element(3);   
    cout<<"search element = "<<n->data<<endl; 
    


